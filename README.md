Termite set up
==============

Installation and configuration of the [termite](https://www.github.com/thestinger/termite) terminal emulator.

Coulour schemes
---------------

Many colour schemes can be tested and downloaded from [terminal.sexy](http://terminal.sexy/).
