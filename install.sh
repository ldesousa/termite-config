#!/bin/bash
# Install instructions stolen from:
# https://computingforgeeks.com/install-termite-terminal-on-ubuntu-18-04-ubuntu-16-04-lts/

# Update system
sudo apt update
sudo apt install build-essential

# Install dependencies
sudo apt-get install -y git g++ libgtk-3-dev gtk-doc-tools gnutls-bin valac intltool libpcre2-dev libglib3.0-cil-dev libgnutls28-dev libgirepository1.0-dev libxml2-utils gperf

# Clone and install VTE
cd ~/git
git clone https://github.com/thestinger/vte-ng.git
echo export LIBRARY_PATH="/usr/include/gtk-3.0:$LIBRARY_PATH"
cd vte-ng
./autogen.sh
make && sudo make install

# Clone and install termite
cd ~/git
git clone --recursive https://github.com/thestinger/termite.git
cd termite
make
sudo make install
sudo ldconfig
sudo mkdir -p /lib/terminfo/x
sudo ln -s /usr/local/share/terminfo/x/xterm-termite /lib/terminfo/x/xterm-termite
sudo update-alternatives --install /usr/bin/x-terminal-emulator x-terminal-emulator /usr/local/bin/termite 60

# Create user configuration file
mkdir -p ~/.config/termite
cp ~/git/termite-config/config ~/.config/termite/config
 
# Make termite the default terminal
gsettings set org.gnome.desktop.default-applications.terminal exec 'termite'

echo "Cleaning up ..."
rm -Rf ~/git/vte-ng
rm -Rf ~/git/termite
